#include <vault.hpp>

void vault::setvars   (const float& shift, const float& tilt, 
                           const float& butterfly, const float& constant)
{
   require_auth (get_self());

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());
   c.shift = shift;
   c.tilt = tilt;
   c.butterfly = butterfly;
   c.constant = constant;
   config_s.set(c, get_self());
}

void vault::setprice (const float& staking_token_to_interest_token_price) {
   require_auth (get_self());

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());
   c.staking_token_to_interest_token_price = staking_token_to_interest_token_price;
   config_s.set(c, get_self());
}

void vault::setconfig (const name& staking_token_contract, const symbol& staking_token_symbol,
                        const name& interest_token_contract, const symbol& interest_token_symbol) {

   require_auth (get_self());

   check (is_account(staking_token_contract), "Staking token contract " + 
      staking_token_contract.to_string() + " is not a valid account.");

   check (is_account(interest_token_contract), "Interest token contract " + 
      interest_token_contract.to_string() + " is not a valid account.");

   check (staking_token_symbol.is_valid(), "Staking token symbol " + 
      staking_token_symbol.code().to_string() + " is not a valid symbol.");

   check (interest_token_symbol.is_valid(), "Interest token symbol " + 
      interest_token_symbol.code().to_string() + " is not a valid symbol.");

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());
   c.staking_token_contract = staking_token_contract;
   c.staking_token_symbol = staking_token_symbol;
   c.interest_token_contract = interest_token_contract;
   c.interest_token_symbol = interest_token_symbol;
   config_s.set(c, get_self());
}

void vault::deposit (const name& from, const name& to, const asset& quantity, const string& memo) {

   if (to != get_self()) { return; }
   if (memo == "NODEPOSIT") { return; }   // use memo of NODEPOSIT to transfer without depositing

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());

   check (! is_paused(), "Vault contract is paused. Try again later.");
   check (c.staking_token_symbol == quantity.symbol, "Only tokens of the staking symbol allowed. You sent " +
      quantity.symbol.code().to_string() + "; Staking symbol: " + c.staking_token_symbol.code().to_string());

   check (c.staking_token_contract == get_first_receiver(), "Only tokens of the staking contract allowed. You sent from " +
      get_first_receiver().to_string() + "; Valid staking token contract: " + c.staking_token_contract.to_string());

   balance_table balances(get_self(), from.value);
   asset new_balance;
   auto it = balances.find(quantity.symbol.code().raw());
   if(it != balances.end()) {
      check (it->token_contract == get_first_receiver(), "Transfer does not match existing token contract.");
      balances.modify(it, get_self(), [&](auto& bal){
         bal.funds += quantity;
         new_balance = bal.funds;
      });
   }
   else {
      balances.emplace(get_self(), [&](auto& bal){
         bal.funds = quantity;
         bal.token_contract  = get_first_receiver();
         new_balance = quantity;
      });
   }

   print ("\n");
   print(name{from}, " deposited:       ", quantity, "\n");
   print(name{from}, " funds available: ", new_balance);
   print ("\n");
}


void vault::setsetting ( const name& setting_name, const uint8_t& setting_value ) {
   require_auth (get_self());

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());
   c.settings[setting_name] = setting_value;
   config_s.set(c, get_self());
}

void vault::pause () {
   setsetting ("active"_n, 0);
}

void vault::activate () {
   setsetting ("active"_n, 1);
}

void vault::stake (const name& account, const asset& quantity, const uint16_t& staked_duration_days) {
   
   check (! is_paused(), "Vault contract is paused. Try again later.");

   config_table config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());

   auto staked_years = (float) staked_duration_days / (float) 365;
   auto staked_years_sq = staked_years*staked_years;
   auto butterfly_stake_sq = c.butterfly * (staked_years*staked_years);
   auto tilt_x_years = c.tilt * staked_years;
   auto interest_numerator = c.shift + (c.tilt * staked_years) + (c.butterfly * (staked_years*staked_years));
   auto duration_interest_rate = (float) interest_numerator / 100; 
   
   print ("\nStaked years        : ", std::to_string(staked_years), "\n");
   print ("Staked years sq       : ", std::to_string(staked_years_sq), "\n");
   print ("Butterfly stake sq    : ", std::to_string(butterfly_stake_sq), "\n");
   print ("Tilt X years          : ", std::to_string(tilt_x_years), "\n");
   print ("Interest Numerator    : ", std::to_string(interest_numerator), "\n\n");

   auto asset_amount = (float) quantity.amount / (float) pow (10, quantity.symbol.precision());
   auto log_size  = (float) log10(asset_amount);
   auto log_size_x_constant = (float) c.constant * (float) log_size;
   auto max_vs_constant = std::max ( (float) 0.00, (float) log_size_x_constant);
   auto size_interest_rate = (float) max_vs_constant / (float) 100;

   print ("Asset amount          : ", std::to_string(asset_amount), "\n");
   print ("Log size x constant   : ", std::to_string(log_size_x_constant), "\n");
   print ("Max vs constant       : ", std::to_string(max_vs_constant), "\n");
   print ("Size interest rate    : ", std::to_string(size_interest_rate), "\n");

   auto interest_amount = asset_amount * c.staking_token_to_interest_token_price;
   auto interest_asset  = asset { static_cast<int64_t>(interest_amount * pow (10, c.interest_token_symbol.precision())), c.interest_token_symbol };

   position_table p_t (get_self(), get_self().value);
   p_t.emplace (get_self(), [&](auto &p) {
      p.position_id                          = p_t.available_primary_key();
      p.position_owner                       = account;
      p.staked_asset                         = quantity;
      p.staked_asset_interest_token_value    = interest_asset;
      p.position_expiration_time             = time_point_sec(current_time_point().sec_since_epoch() + staked_duration_days * 24 * 60 * 60);
      p.interest_rate                        = duration_interest_rate + size_interest_rate;
      p.interest_paid                        = asset { 0, c.interest_token_symbol };
   });
}

void vault::unstake (const uint64_t& position_id) {
   check (! is_paused(), "Vault contract is paused. Try again later.");

   position_table p_t (get_self(), get_self().value);
   auto p_itr = p_t.find (position_id);
   check (p_itr != p_t.end(), "Position ID is not found: " + std::to_string(position_id));
   require_auth (p_itr->position_owner);

   // confirm that expiration date has passed
   check (current_time_point().sec_since_epoch() >= p_itr->position_expiration_time.sec_since_epoch(),
      "Cannot unstake. Position has not yet expired.");

   if (p_itr->last_interest_paid_time < p_itr->position_expiration_time) {
      claim (position_id);
   }

   p_t.erase (p_itr);
}

void vault::withdraw (const name& position_owner, const asset& quantity) {
   require_auth (position_owner);
   check (! is_paused(), "Vault contract is paused. Try again later.");   

   asset available_balance = get_available_balance (position_owner);
   check (available_balance >= quantity, "Insufficient funds. You requested " +
      quantity.to_string() + " but your available balance is only " + available_balance.to_string());

   balance_table balances(get_self(), position_owner.value);
   auto it = balances.find(quantity.symbol.code().raw());
   balances.modify(it, get_self(), [&](auto& bal){
      bal.funds -= quantity;
   });

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());

   string send_memo { "Withdrawal from Vault" };
   action(
      permission_level{get_self(), "active"_n},
      c.staking_token_contract, "transfer"_n,
      std::make_tuple(get_self(), position_owner, quantity, send_memo))
   .send();
}

void vault::withdrawall (const name& position_owner) {
   require_auth (position_owner);
   withdraw (position_owner, get_available_balance (position_owner));
}

void vault::claim (const uint64_t& position_id) {
   check (! is_paused(), "Vault contract is paused. Try again later.");
   position_table p_t (get_self(), get_self().value);
   auto p_itr = p_t.find (position_id);
   check (p_itr != p_t.end(), "Position ID is not found: " + std::to_string(position_id));
   require_auth (p_itr->position_owner);

   // confirm that there is interest left to be paid
   check (p_itr->last_interest_paid_time < p_itr->position_expiration_time,
      "Nothing to do. Position has expired and all interest has been claimed. You should unstake it. Position #" +
      std::to_string(position_id));

   // seconds since interest was last claimed
   uint32_t seconds_accrued = std::min (p_itr->position_expiration_time.sec_since_epoch(), current_time_point().sec_since_epoch()) - 
      std::max (p_itr->last_interest_paid_time.sec_since_epoch(), p_itr->position_staked_time.sec_since_epoch());
   print (" Seconds accrued         : ", std::to_string(seconds_accrued), "\n");

   print (" SECONDS_PER_YEAR        : ", std::to_string(SECONDS_PER_YEAR), "\n");
   float share_of_year = (float) (seconds_accrued * SCALER) / (float) (SECONDS_PER_YEAR * SCALER);
   print (" Share of year    : ", std::to_string(share_of_year), "\n");

   float interest_perc_due = (float) p_itr->interest_rate * (float) share_of_year;
   print (" Interest perc due       : ", std::to_string(interest_perc_due), "\n");

   asset interest_to_pay = adjust_asset(p_itr->staked_asset_interest_token_value, interest_perc_due);
   print (" Interest to Pay         : ", interest_to_pay.to_string(), "\n");

   p_t.modify (p_itr, get_self(), [&](auto &p) {
      p.interest_paid += interest_to_pay;
      p.last_interest_paid_time = time_point_sec(current_time_point());
   });

   config_table      config_s (get_self(), get_self().value);
   config c = config_s.get_or_create (get_self(), config());

   string send_memo { "Interest Payment from Position #" + std::to_string(position_id) };
   action(
      permission_level{get_self(), "active"_n},
      c.interest_token_contract, "transfer"_n,
      std::make_tuple(get_self(), p_itr->position_owner, interest_to_pay, send_memo))
   .send();
}

void vault::claimall (const name& account) {
   require_auth (account);
   check (! is_paused(), "Vault contract is paused. Try again later.");

   position_table p_t (get_self(), get_self().value);
   auto owner_index = p_t.get_index<"byowner"_n>();
   auto owner_itr = owner_index.find (account.value);

   while (owner_itr != owner_index.end() && owner_itr->position_owner == account) {
      claim (owner_itr->position_id);
      owner_itr++;
   }
}

void vault::rewind (const uint64_t& position_id, const uint32_t& rewind_days) {
   position_table p_t (get_self(), get_self().value);
   auto p_itr = p_t.find (position_id);
   check (p_itr != p_t.end(), "Position ID is not found: " + std::to_string(position_id));
   require_auth (get_self());

   p_t.modify (p_itr, get_self(), [&](auto &p) {
      p.position_staked_time -= (60 * 60 * 24 * rewind_days);
   });
}